#!/bin/bash
# shellcheck disable=SC2046

# 将两个sh文件复制到 ~/ 路径下
cp $(rospack find auto_start)/scripts/start_master.sh ~/
cp $(rospack find auto_start)/scripts/hello_world_start.sh ~/

# 将两个sh文件权限，使其具有可执行的权限
chmod 777 ~/start_master.sh
chmod 777 ~/hello_world_start.sh

# 将两个.service 文件拷贝到相应的系统文件夹
sudo cp $(rospack find auto_start)/scripts/start_master.service  /lib/systemd/system/
sudo cp $(rospack find auto_start)/scripts/hello_world_start.service  /lib/systemd/system/

# 开启这两个服务的开机自启动
sudo systemctl enable start_master.service
sudo systemctl enable hello_world_start.service
echo "Finish "
